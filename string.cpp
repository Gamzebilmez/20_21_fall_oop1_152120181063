#include <iostream>
#include <string>
using namespace std;

int main() {
   
    string str1;
    string str2;
    getline(cin, str1);
    getline(cin, str2);
    cout << str1.size() << ' ' << str2.size() << endl;
    cout << str1 + str2 << endl;
    string str3 = str1;
    string str4 = str2;
    str3[0] = str2[0];
    str4[0] = str1[0];
    cout << str3 << ' ' << str4 << endl;

    return 0;
}