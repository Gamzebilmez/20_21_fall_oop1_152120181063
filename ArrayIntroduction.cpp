#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int size;
    int ARRAY[1000];

    cin >> size;
    if (size < 1 || size>1000) {
        cout << "the size out of range";
        exit(0);
    }
    for (int i = 0; i < size; i++) {
        cin >> ARRAY[i];
    }
    for (int i = size - 1; i >= 0; i--) {
        cout << ARRAY[i] << " ";
    }

    return 0;
}
