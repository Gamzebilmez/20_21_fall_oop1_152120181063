#include <stdio.h>

void update(int* a, int* b) {
    int sum;
    int substract;
    sum = *a + *b;
    substract = *a - *b;
    if (substract < 0)
        substract = substract * (-1);

    *a = sum;
    *b = substract;
}

int main() {
    int a, b;
    int* pa = &a, * pb = &b;

    scanf("%d %d", &a, &b);
    update(pa, pb);
    printf("%d\n%d", a, b);

    return 0;
}