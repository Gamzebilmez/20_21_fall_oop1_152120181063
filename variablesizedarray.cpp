#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {

    int n, q;
    cin >> n >> q;
    int k, i, j;
    vector<vector<int> >ARRAY(n);
    for (i = 0; i < n; i++)
    {
        cin >> k;
        ARRAY[i].resize(k);
        for (j = 0; j < k; j++)
        {
            cin >> ARRAY[i][j];
        }
    }

    for (int m = 0; m < q; m++)
    {
        cin >> i >> j;
        cout << ARRAY[i][j] << endl;
    }


    return 0;
}
