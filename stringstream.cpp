#include <sstream>
#include <vector>
#include <iostream>
using namespace std;

vector<int> parseInts(string str) {
    stringstream ss(str);
    int x;
    vector<int> vec;

    while (ss >> x) {
        vec.push_back(x);
        if (ss.peek() == ',') {
            ss.ignore();
        }
    }

    return vec;
}

int main() {
    string str;
    cin >> str;
    if (str.size() > 8 * 100000)
    {
        cout << "string size out of range";
        exit(0);
    }
    vector<int> integers = parseInts(str);
    for (int i = 0; i < integers.size(); i++) {
        cout << integers[i] << "\n";
    }

    return 0;
}